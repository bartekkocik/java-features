package java16.patternMatchingInstanceOf;

public class PatternMatchingInstanceOf {

    public static void main(String[] args) {
        Object myString = new MyObject("value");


        // order of checking conditions is default
        if (myString instanceof MyObject myObject && myObject.getValue() != null) {
            System.out.println(myObject.getValue());
//            System.out.println(myString.getValue());
        }

        if (!(myString instanceof MyObject myNewObject)) {
            throw new RuntimeException("not my instance");
        }
        myNewObject.getValue(); // compiler is sure this object exist


        System.out.println(formatter("test"));
    }

    static String formatter(Object o) {
        String formatted = "unknown";
        if (o instanceof Integer i) {
            formatted = String.format("int %d", i);
        } else if (o instanceof Long l) {
            formatted = String.format("long %d", l);
        } else if (o instanceof Double d) {
            formatted = String.format("double %f", d);
        } else if (o instanceof String s) {
            formatted = String.format("String %s", s);
        }
        return formatted;
    }

    private static class MyObject {

        private String value;

        public MyObject(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }

    }
}
