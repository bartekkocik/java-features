package java12.switchExpression;

/**
 * preview, TODO which release introduced feature
 */
public class SwitchExpression {
    public static void main(String[] args) {
        MyEnumValue value = MyEnumValue.A;
        switch (value) {
            case A:
                System.out.println("A without break");
                // purposely missing break
            case B:
                System.out.println("B without break");
                // purposely missing break
            default:
                System.out.println("undefined");
        }

        final String valueReturnedBySwitch = switch (value) {
            case A -> getString();
            case B -> "B without break";
            case C -> null;
            default -> throw new RuntimeException("hola hola");
        };
        System.out.println(valueReturnedBySwitch);

        System.out.println(switch (value) {
            case A -> {
                System.out.println("the given value was: " + value);
                yield "A";
            }
            case B -> "B";
            default -> "default";
        });

    }

    private static String getString() {
        return "A without break";
    }

    enum MyEnumValue {
        A, B, C
    }
}
