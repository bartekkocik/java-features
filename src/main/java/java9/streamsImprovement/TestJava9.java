package java9.streamsImprovement;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class TestJava9 {
    public static void main(String[] args) {

        final List<String> a = List.of("a");
        Map.of(1, "one", 2, "two");

        final Optional<String> test = Optional.of("test");

        // stream is new
        final long count = test.stream().count();

        final Optional<String> test2 = Optional.empty();
        test2.ifPresentOrElse(s -> System.out.println("test2"), () -> System.out.println("empty"));

        // improvement in stream
        Stream.of(1, 2, 3, 4, 5, 6)
                .takeWhile(i -> i < 4)
                .forEach(System.out::println);


        Stream.of(1, 2, 3, 4, 5)
                .dropWhile(x -> x < 4)
                .forEach(System.out::println);

        IntStream.iterate(2, x -> x < 20, x -> x * x)
                .forEach(System.out::println);
    }

}

