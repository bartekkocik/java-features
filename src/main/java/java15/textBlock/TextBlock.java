/*
 * Parts Inspection and Approval
 * Copyright © 2021 Mercedes-Benz AG
 */

package java15.textBlock;

public class TextBlock {

    public static void main(String[] args) {
        final String x = """
                wielolinijkowy teskst
                w który każda zmiana linie to \r\n
                dla windows i \n dla unixe.
                """;
        System.out.println(x);

        System.out.println("""
            {
              "name": "John Doe",
              "age": 45,
              "address": "Doe Street, 23, Java Town"
            }
            """);
    }
}
