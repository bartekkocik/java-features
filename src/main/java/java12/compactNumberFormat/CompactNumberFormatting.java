package java12.compactNumberFormat;

import java.text.NumberFormat;
import java.util.Locale;

public class CompactNumberFormatting {

    public static void main(String[] args) {
        NumberFormat fmt = NumberFormat.getCompactNumberInstance(Locale.getDefault(), NumberFormat.Style.SHORT);
        System.out.println(fmt.format(1000));
        System.out.println(fmt.format(100000));
        System.out.println(fmt.format(1000000));

        System.out.println("---");

        fmt = NumberFormat.getCompactNumberInstance(Locale.getDefault(), NumberFormat.Style.LONG);
        System.out.println(fmt.format(1000));
        System.out.println(fmt.format(100000));
        System.out.println(fmt.format(1000000));

        System.out.println("---");

        fmt = NumberFormat.getCompactNumberInstance(Locale.forLanguageTag("PL"), NumberFormat.Style.LONG);
        System.out.println(fmt.format(1000));
        System.out.println(fmt.format(100000));
        System.out.println(fmt.format(1000000));


    }
}
