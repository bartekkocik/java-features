package java11.stringImprovement;

public class StringImprovement {
    public static void main(String[] args) {
        String stringValue = " aaa ";
        if (!stringValue.isBlank()) {
            System.out.println("not blank");
        }
        System.out.println("\'" + stringValue.strip() + "\'");
        System.out.println("\'" + stringValue.stripLeading() + "\'");
        System.out.println("\'" + stringValue.stripTrailing() + "\'");

        // Klasa Files wzbogaciła się o metody takie jak: writeString() i readString(),
        // które zapisują i odczytują zawartość pliku w postaci stringa. Dodano też
        // metodę isSameFile(), która sprawdza, czy dane pliki są takie same.

    }
}
