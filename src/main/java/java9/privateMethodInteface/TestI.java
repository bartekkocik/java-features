package java9.privateMethodInteface;

public interface TestI {
    private void privateMethodInInterface() {
        System.out.println();
    }
}
