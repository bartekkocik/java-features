package java14.npe;

public class NPE {

    public static void main(String[] args) {
        MyLocalObject myLocalObject = null;
        System.out.println(myLocalObject.testMe());
        // in java11 there was not because segment
    }

    private class MyLocalObject {
        public String testMe() {
            return "result";
        }
    }
}
