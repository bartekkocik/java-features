package java10.localVar;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import static java.util.Arrays.asList;

public class LocalVar {
    public static void main(String[] args) {
        // for local var
        var hello = "Hello world";
        System.out.println(hello);


        final List<String> a = List.copyOf(asList("a"));
        System.out.println(a);

        final Set<String> s = Set.copyOf(Set.of("a"));
        System.out.println(s);

        final Map<Integer, String> m = Map.copyOf(Map.of(1, "a"));
        System.out.println(m);

        final List<String> collect = a.stream().collect(Collectors.toUnmodifiableList());
        final Set<String> collect1 = s.stream().collect(Collectors.toUnmodifiableSet());
        final Map<String, Integer> collect2 = s.stream().collect(Collectors.toUnmodifiableMap(Function.identity(),
                String::length));
        System.out.println(collect);
        System.out.println(collect1);
        System.out.println(collect2);

    }
}
