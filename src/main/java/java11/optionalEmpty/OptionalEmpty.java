package java11.optionalEmpty;

import java.util.Optional;

public class OptionalEmpty {
    public static void main(String[] args) {

        // from java 11
        final Optional<Object> empty = Optional.empty();

    }
}
