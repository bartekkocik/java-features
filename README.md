Java 17 Features

Java 17 was released on September 14, 2021. Java 17 is an LTS (Long Term Support) release, like Java 11 and Java 8. Spring 6 and Spring boot 3 will have first-class support for Java 17. So it is a good idea to plan for upgrading to Java 17.

The below listed 14 JEPs are part of Java 17.

    (JEP-306) Restore Always-Strict Floating-Point Semantics
    (JEP-356) Enhanced Pseudo-Random Number Generators
    (JEP-382) New macOS Rendering Pipeline
    (JEP-391) macOS/AArch64 Port
    (JEP-398) Deprecate the Applet API for Removal
    (JEP-403) Strongly Encapsulate JDK Internals
    (JEP-406) Pattern Matching for switch (Preview)
    (JEP-407) Remove RMI Activation
    (JEP-409) Sealed Classes
    (JEP-410) Remove the Experimental AOT and JIT Compiler
    (JEP-411) Deprecate the Security Manager for Removal
    (JEP-412) Foreign Function & Memory API (Incubator)
    (JEP-414) Vector API (Second Incubator)
    (JEP-415) Context-Specific Deserialization Filters

Java 16 Features

Java 16 was released on 16 March 20121. It was largely a maintenance release, except it made the Java Records and Pattern matching the standard features of Java language.

    JEP 338: Vector API (Incubator)
    JEP 347: Enable C++14 Language Features
    JEP 357: Migrate from Mercurial to Git
    JEP 369: Migrate to GitHub
    JEP 376: ZGC: Concurrent Thread-Stack Processing
    JEP 380: Unix-Domain Socket Channels
    JEP 386: Alpine Linux Port
    JEP 387: Elastic Metaspace
    JEP 388: Windows/AArch64 Port
    JEP 389: Foreign Linker API (Incubator)
    JEP 390: Warnings for Value-Based Classes
    JEP 392: Packaging Tool
    JEP 393: Foreign-Memory Access API (Third Incubator)
    JEP 394: Pattern Matching for instanceof
    JEP 395: Records
    JEP 396: Strongly Encapsulate JDK Internals by Default
    JEP 397: Sealed Classes (Second Preview)

Java 15 Features

Java 15 was released on 15th Sep’2020. It continues support for various preview features in previous JDK releases; and also introduced some new features.

    Sealed Classes and Interfaces (Preview) (JEP 360)
    EdDSA Algorithm (JEP 339)
    Hidden Classes (JEP 371)
    Pattern Matching for instanceof (Second Preview) (JEP 375)
    Removed Nashorn JavaScript Engine (JEP 372)
    Reimplement the Legacy DatagramSocket API (JEP 373)
    Records (Second Preview) (JEP 384)
    Text Blocks become a standard feature. (JEP 378)

Java 14 Features

Java 14 (released on March 17, 2020) is the latest version available for JDK. Let’s see the new features and improvements, it brings for developers and architects.

    JEP 305 – Pattern Matching for instanceof (Preview)
    JEP 368 – Text Blocks (Second Preview)
    JEP 358 – Helpful NullPointerExceptions
    JEP 359 – Records (Preview)
    JEP 361 – Switch Expressions (Standard)
    JEP 343 – Packaging Tool (Incubator)
    JEP 345 – NUMA-Aware Memory Allocation for G1
    JEP 349 – JFR Event Streaming
    JEP 352 – Non-Volatile Mapped Byte Buffers
    JEP 363 – Remove the Concurrent Mark Sweep (CMS) Garbage Collector
    JEP 367 – Remove the Pack200 Tools and API
    JEP 370 – Foreign-Memory Access API (Incubator)

Java 13 Features

Java 13 (released on September 17, 2019) had fewer developer-specific features. Let’s see the new features and improvements, it brought for developers and architects.

    JEP 355 – Text Blocks (Preview)
    JEP 354 – Switch Expressions Enhancements (Preview)
    JEP 353 – Reimplement the Legacy Socket API
    JEP 350 – Dynamic CDS Archive
    JEP 351 – ZGC: Uncommit Unused Memory
    FileSystems.newFileSystem() Method
    DOM and SAX Factories with Namespace Support

Java 12 Features

Java 12 was released on March 19, 2019. Let’s see the new features and improvements, it brings for developers and architects.

    Collectors.teeing() in Stream API
    String API Changes
    Files.mismatch(Path, Path)
    Compact Number Formatting
    Support for Unicode 11
    Switch Expressions (Preview)

Java 11 Features

Java 11 (released on September 2018) includes many important and useful updates. Let’s see the new features and improvements, it brings for developers and architects.

    HTTP Client API
    Launch Single-File Programs Without Compilation
    String API Changes
    Collection.toArray(IntFunction)
    Files.readString() and Files.writeString()
    Optional.isEmpty()

Java 10 Features

After Java 9 release, Java 10 came very quickly. Unlike it’s previous release, Java 10 does not have that many exciting features, still, it has few important updates which will change the way you code, and other future Java versions.

    JEP 286: Local Variable Type Inference
    JEP 322: Time-Based Release Versioning
    JEP 304: Garbage-Collector Interface
    JEP 307: Parallel Full GC for G1
    JEP 316: Heap Allocation on Alternative Memory Devices
    JEP 296: Consolidate the JDK Forest into a Single Repository
    JEP 310: Application Class-Data Sharing
    JEP 314: Additional Unicode Language-Tag Extensions
    JEP 319: Root Certificates
    JEP 317: Experimental Java-Based JIT Compiler
    JEP 312: Thread-Local Handshakes
    JEP 313: Remove the Native-Header Generation Tool
    New Added APIs and Options
    Removed APIs and Options

Java 9 Features

Java 9 was made available on September, 2017. The biggest change is the modularization i.e. Java modules.

Some important features/changes in Java 9 are:

    Java platform module system
    Interface Private Methods
    HTTP 2 Client
    JShell – REPL Tool
    Platform and JVM Logging
    Process API Updates
    Collection API Updates
    Improvements in Stream API
    Multi-Release JAR Files
    @Deprecated Tag Changes
    Stack Walking
    Java Docs Updates
    Miscellaneous Other Features

Please see the updated release info here.
Java 8 Features

Release Date : March 18, 2014

Code name culture is dropped. Included features were:

    Lambda expression support in APIs
    Stream API
    Functional interface and default methods
    Optionals
    Nashorn – JavaScript runtime which allows developers to embed JavaScript code within applications
    Annotation on Java Types
    Unsigned Integer Arithmetic
    Repeating annotations
    New Date and Time API
    Statically-linked JNI libraries
    Launch JavaFX applications from jar files
    Remove the permanent generation from GC

Java SE 7 Features

Release Date : July 28, 2011

This release was called “Dolphin”. Included features were:

    JVM support for dynamic languages
    Compressed 64-bit pointers
    Strings in switch
    Automatic resource management in try-statement
    The diamond operator
    Simplified varargs method declaration
    Binary integer literals
    Underscores in numeric literals
    Improved exception handling
    ForkJoin Framework
    NIO 2.0 having support for multiple file systems, file metadata and symbolic links
    WatchService
    Timsort is used to sort collections and arrays of objects instead of merge sort
    APIs for the graphics features
    Support for new network protocols, including SCTP and Sockets Direct Protocol

Java SE 6 Features

Release Date : December 11, 2006

This release was called “Mustang”. Sun dropped the “.0” from the version number and version became Java SE 6. Included features were:

    Scripting Language Support
    Performance improvements
    JAX-WS
    JDBC 4.0
    Java Compiler API
    JAXB 2.0 and StAX parser
    Pluggable annotations
    New GC algorithms

J2SE 5.0 Features

Release Date : September 30, 2004

This release was called “Tiger”. Most of the features, which are asked in java interviews, were added in this release.

Version was also called 5.0 rather than 1.5. Included features are listed down below:

    Generics
    Annotations
    Autoboxing/unboxing
    Enumerations
    Varargs
    Enhanced for each loop
    Static imports
    New concurrency utilities in java.util.concurrent
    Scanner class for parsing data from various input streams and buffers.

J2SE 1.4 Features

Release Date : February 6, 2002

This release was called “Merlin”. Included features were:

    assert keyword
    Regular expressions
    Exception chaining
    Internet Protocol version 6 (IPv6) support
    New I/O; NIO
    Logging API
    Image I/O API
    Integrated XML parser and XSLT processor (JAXP)
    Integrated security and cryptography extensions (JCE, JSSE, JAAS)
    Java Web Start
    Preferences API (java.util.prefs)

J2SE 1.3 Features

Release Date : May 8, 2000

This release was called “Kestrel”. Included features were:

    HotSpot JVM
    Java Naming and Directory Interface (JNDI)
    Java Platform Debugger Architecture (JPDA)
    JavaSound
    Synthetic proxy classes

J2SE 1.2 Features

Release Date : December 8, 1998

This release was called “Playground”. This was a major release in terms of number of classes added (almost trippled the size). “J2SE” term was introduced to distinguish the code platform from J2EE and J2ME. Included features were:

    strictfp keyword
    Swing graphical API
    Sun’s JVM was equipped with a JIT compiler for the first time
    Java plug-in
    Collections framework

JDK 1 Features

Release Date : January 23, 1996

This was the initial release and was originally called Oak. This had very unstable APIs and one java web browser named WebRunner.

The first stable version, JDK 1.0.2, was called Java 1.

On February 19, 1997, JDK 1.1 was released having a list of big features such as:

    AWT event model
    Inner classes
    JavaBeans
    JDBC
    RMI
    Reflection which supported Introspection only, no modification at runtime was possible.
    JIT (Just In Time) compiler for Windows