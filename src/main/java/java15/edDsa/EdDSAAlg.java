package java15.edDsa;

import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.Signature;
import java.security.SignatureException;
import java.util.Base64;

/**
 * Elliptic Curve Digital Signature Algorithm
 *
 * Description
 * EdDSA
 *
 * Edwards-Curve signature algorithm as defined in RFC 8032
 * Ed25519
 *
 * Edwards-Curve signature algorithm with Ed25519 as defined in RFC 8032
 * Ed448
 *
 * Edwards-Curve signature algorithm with Ed448 as defined in RFC 8032
 */
public class EdDSAAlg {
    public static void main(String[] args)
            throws NoSuchAlgorithmException, InvalidKeyException, SignatureException {

        KeyPairGenerator kpg = KeyPairGenerator.getInstance("Ed25519");
        KeyPair kp = kpg.generateKeyPair();

        byte[] msg = "test_string".getBytes(StandardCharsets.UTF_8);

        Signature sig = Signature.getInstance("Ed25519");
        sig.initSign(kp.getPrivate());
        sig.update(msg);
        byte[] s = sig.sign();

        String encodedString = Base64.getEncoder().encodeToString(s);
        System.out.println(encodedString);
    }
}
