package java16.record;

public class RecordExample {

    public static void main(String[] args) {

        record RecordB(String valueA, String valueB) {

        }

        final RecordA recordA1 = new RecordA((short) 1, (short) 2);
        System.out.println(recordA1);
        final RecordA recordA2 = new RecordA((short) 1, (short) 2);
        System.out.println(recordA1.equals(recordA2));

        final RecordB recordB = new RecordB("a", "b");
        System.out.println(recordB);

    }

    private record RecordA(Short valueA, Short valueB) {

        public RecordA(Short valueA) {
            this(valueA, null);
        }
    }
}
